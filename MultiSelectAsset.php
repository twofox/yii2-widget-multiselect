<?php
namespace twofox\multiselect;

/**
 * Asset bundle for DatePicker Widget
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 1.0
 */
class MultiSelectAsset extends \yii\web\AssetBundle
{
    public $css = [
        'css/multi-select.css'
    ];
    
    public $js = [
        'js/jquery.multi-select.js'
    ];
    
    public function init()
    {
        $this->sourcePath =  __DIR__ . '/assets';
        parent::init();
    }
}
