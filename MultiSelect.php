<?php

/**
 * @copyright Copyright &copy; Kartik Visweswaran, Krajee.com, 2014 - 2016
 * @package yii2-widgets
 * @subpackage yii2-widget-datepicker
 * @version 1.3.9
 */

namespace twofox\multiselect;

use Yii;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Json;
use yii\web\View;

/**
 * DatePicker widget is a Yii2 wrapper for the Bootstrap DatePicker plugin. The plugin is a fork of Stefan Petre's
 * DatePicker (of eyecon.ro), with improvements by @eternicode.
 *
 * @author Kartik Visweswaran <kartikv2@gmail.com>
 * @since 1.0
 * @see http://eternicode.github.io/bootstrap-datepicker/
 */
class MultiSelect extends \yii\widgets\InputWidget
{   

    
     public $data = []; 
     public $options = [];
     public $pluginOptions = [];
     
     /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $view = $this->getView();
        MultiSelectAsset::register($view);        
        $id = 'multiselect_'.hash('crc32', rand(0, 9999).time().'multiselect'); 
                
        if($this->hasModel())
            echo Html::activeDropDownList($this->model, $this->attribute, $this->data, array_merge(['multiple'=>true, 'class'=>$id, 'size'=>20], $this->options));
        else
            echo Html::dropDownList($this->name, $this->value, $this->data, array_merge(['multiple'=>true, 'class'=>$id, 'size'=>20], $this->options));
        
                
        $this->pluginOptions = Json::encode($this->pluginOptions);
        $view->registerJs("var {$id} = {$this->pluginOptions};", View::POS_HEAD);
                
        $view->registerJs("
            $('.{$id}').multiSelect({$id});        
        ");
    }
}
